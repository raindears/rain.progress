define([], function () {
  'use strict';

  /**
   * @name Progress
   * @class A progress bar with textual representation of the current value.
   *
   * <p>In a template you can use the following attributes with the component helper:</p>
   *
   * <ul>
   *   <li><code>max</code>: The maximum value (default: 0, in contrast to HTML5 progress element)</li>
   *   <li><code>value</code>: The initial value (default: 1)</li>
   *   <li><code>text</code>: The textual representation of the initial value (optional)</li>
   * </ul>
   *
   * @example
   * {{component name="progress" max=100 value=60}}
   *
   * // Since <code>max</code> is 1 by default, this is equivalent to:
   * {{component name="progress" value=0.6}}
   */
  function Progress() {}

  Progress.prototype =
  /** @lends Progress# */
  {

    /**
     * Init lifecycle method
     */
    init: function () {},

    $el: null,
    $bar: null,
    $text: null,

    /**
     * Start lifecycle method
     */
    start: function () {
      this.$el = this.context.getRoot().find('.progress');
      this.$bar = this.$('.bar');
      this.$text = this.$('span');

      this.render();
    },

    /**
     * Run queries within this view's DOM.
     *
     * @param {String} selector A jQuery selector
     * @returns {jQuery} A jQuery collection
     */
    $: function(selector) {
      return this.$el.find(selector);
    },

    /**
     * Get or set the maximum value.
     *
     * @param {Number} [max] The maximum value
     * @returns {Number|AsyncController} The maximum value or <code>this</code>
     */
    max: function(max) {
      var valueMax = attr('aria-valuemax').bind(this);

      if (arguments.length > 0) {
        if (isNaN(max) || ! isFinite(max) || max <= 0) {
          return this;
        }

        var value = this.value();

        if (max < value) {
          max = value;
        }

        return valueMax(max);
      }

      return valueMax();
    },

    /**
     * Get or set the current value.
     *
     * @param {Number} [value] The current value
     * @returns {Number|AsyncController} The current value or <code>this</code>
     */
    value: function(value) {
      var valueNow = attr('aria-valuenow').bind(this);

      if (arguments.length > 0) {
        if (isNaN(value) || ! isFinite(value)) {
          return this;
        }

        if (value < 0) {
          value = 0;
        }

        var max = this.max();

        if (value > max) {
          value = max;
        }

        return valueNow(value);
      }


      return valueNow();
    },

    _tplText: null,

    /**
     * Get or set the textual representation of the current value.
     *
     * @param {String} [text] The text to set or a format string with placeholders: {{max}}, {{value}}, {{percent}}
     * @param {Boolean} [live=false] Whether the text should update automatically,
     *     when <code>value</code> or <code>max</code> change
     * @returns {String|AsyncController} The current text or <code>this</code>
     */
    text: function(text, live) {
      if (arguments.length > 0) {
        this._tplText = text;
        this.render();

        if (! live) {
          this._tplText = null;
        }

        return this;
      }

      return this.$text.text();
    },

    render: function() {
      var max = this.max()
        , value = this.value()
        , percent = progress(max, value);

      this.$bar.css('width', percent);

      if (this._tplText) {
        var text = this._tplText
            .replace('{{max}}', max)
            .replace('{{value}}', value)
            .replace('{{percent}}', percent);

        this.$text.text(text);
      }
    }

  };

  return Progress;

  function attr(name) {
    return function(value) {
      if (arguments.length > 0) {
        this.$el.attr(name, value);
        this.render();
        return this;
      }

      return this.$el.attr(name);
    };
  }

  function progress(max, value) {
    return Math.round((value / max) * 100) + '%';
  }

});
